# Poetry

[Poetry](https://python-poetry.org/) es una herramienta para la gestión de dependencias y el empaquetado en Python. Le permite declarar las bibliotecas de las que depende su proyecto y las administrará (instalará / actualizará) por usted.

<!-- #region -->
## Instalación 

Poetry proporciona un instalador personalizado que instalará poetry aislado del resto de su sistema al vender sus dependencias. Esta es la forma recomendada de instalar poesía.

### osx / linux / bashonwindows
```terminal
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

### windows powershell
```terminal
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -
```


## Crear un proyecto con poetry
Poetry agiliza no solo la gestión de dependencias, sino casi todo lo que implica la estructuración de un proyecto Python. Poetry tiene una CLI robusta, que nos permite crear y configurar proyectos Python fácilmente. Así es como se ve empezar de cero:

```terminal
poetry new poetry-tutorial-project
```

Esta es una forma absurdamente conveniente de generar una estructura de carpetas estándar de Python para nuestro nuevo proyecto llamado `poetry-tutorial-project`:

```
/poetry-tutorial-project
├── README.md
├── poetry_tutorial_project
│   └── __init__.py
├── pyproject.toml
└── tests
    ├── __init__.py
    └── test_poetry_tutorial_project.py
```

Esto nos ahorra la molestia de crear manualmente esta estructura de carpetas estándar nosotros mismos. La mayor parte del contenido del archivo está vacío, con una excepción: `pyproject.toml`.

### Una configuración para manejar todo

Cada proyecto de Poetry está contenida en un archivo llamado `pyproject.toml`. Aquí es donde definimos todo, desde los metadatos, las dependencias, los scripts y más de nuestro proyecto. Si está familiarizado con Node, piense en pyproject.toml como el equivalente en Python de package.json.

Comenzar un nuevo proyecto de poesía crea automáticamente una versión mínima de este archivo. 

```toml
[tool.poetry]
name = "poetry-tutorial-project"
version = "0.1.0"
description = ""
authors = ["Todd Birchard <todd@example.com>"]

[tool.poetry.dependencies]
python = "^3.7"

[tool.poetry.dev-dependencies]
pytest = "^4.6"

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"
```

La configuración anterior tiene información básica, pero aún no es suficiente para ser útil. Un archivo `pyproject.toml` completo se vería así:

```toml
[tool.poetry]
name = "poetry_tutorial_project"
version = "0.1.0"
description = "Simple Python project built with Poetry."
authors = ["Todd Birchard <toddbirchard@gmail.com>"]
maintainers = ["Todd Birchard <toddbirchard@gmail.com>"]
license = "MIT"
readme = "README.md"
homepage = ""
repository = "https://github.com/hackersandslackers/python-poetry-tutorial/"
documentation = "https://hackersandslackers.com/python-poetry/"
keywords = ["Poetry",
            "Virtual Environments",
            "Tutorial",
            "Packages",
            "Packaging"]

[tool.poetry.dependencies]
python = "^3.7"
loguru = "*"
psutil = "*"

[tool.poetry.dev-dependencies]
pytest = "*"

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"

[tool.poetry.scripts]
run = "wsgi:main"

[tool.poetry.urls]
issues = "https://github.com/hackersandslackers/python-poetry-tutorial/issues"
```

Ahora estamos cocinando con gas! Nuestro archivo .toml ahora se compone de 6 secciones, donde cada sección contiene valores de configuración para nuestro proyecto:

* **[tool.poetry]**: La primera sección de pyproject.toml es simplemente metadatos informativos sobre nuestro paquete, como el nombre del paquete, la descripción, los detalles del autor, etc. La mayoría de los valores de configuración aquí son opcionales a menos que esté planeando publicar esto proyecto como paquete oficial de PyPi. Proporcionar valores para el repositorio y las palabras clave no importará si no distribuye este paquete. Aún así, este tipo de metadatos sería fundamental si alguna vez espera distribuir su paquete.
<!-- #endregion -->

* **[tool.poetry.dependencies]**: Aquí es donde definimos las dependencias que nuestra aplicación debe descargar para ejecutarse. Puede especificar números de versión específicos para los paquetes requeridos (como `Flask = "1.0.0"`), o si simplemente desea obtener la última versión, configurar la versión en "*" hará precisamente eso. También notará que la versión de Python a la que nos dirigimos para nuestro proyecto también se proporciona aquí: esto especifica la versión mínima requerida para ejecutar nuestra aplicación. En el ejemplo anterior, un usuario que ejecute Python 3.6 no podrá ejecutar esta aplicación, ya que especificamos que Python 3.7 es la versión más baja requerida.


* **[tool.poetry.dev-dependencies]**: las dependencias de desarrollo son paquetes que los desarrolladores que contribuyen deben descargar para iterar en este proyecto. Las dependencias de desarrollo no son necesarias para ejecutar la aplicación y no se descargarán cuando la aplicación se compile de forma predeterminada.


* **[build-system]**: Rara vez es una sección que necesitarás tocar a menos que actualices tu versión de poetry.


* **[tool.poetry.scripts]**: Aquí es donde especificamos dónde están los puntos de entrada de nuestra aplicación asignando una función dentro de los módulos al nombre de un script que se ejecutará. El ejemplo `run = "wsgi: main"` especifica que queremos crear un comando llamado "run", que buscará en **wsgi.py** una función llamada **main()**. Con este conjunto, podemos lanzar nuestra aplicación a través de la poetry CLI escribiendo `poetry run` (más sobre esto en un momento).


<!-- #region -->
* **[tool.poetry.urls]**: Esta es una sección completamente opcional donde puede agregar cualquier número de enlaces o recursos útiles que alguien que descargue este paquete pueda encontrar útil.

Una configuración como la anterior es más que suficiente para tener una aplicación empaquetada limpia y funcional. Poetry también admite otros tipos de valores de configuración, aunque es probable que rara vez los necesite. 


### Poetry CLI

La interfaz de línea de comandos de Poetry es impresionantemente simplista para el alcance de lo que logra. Poetry cubre la funcionalidad equivalente de **Pipenv** y **setup.py**, así como muchas otras características relacionadas con la administración de la configuración y la publicación de paquetes. Comenzaremos instalando y administrando las dependencias que acabamos de configurar en `pyproject.toml`.

### Instalación y gestión de dependencias
<!-- #endregion -->

* **`poetry shell`**: la primera vez que se ejecuta este comando en el directorio de su proyecto, Poetry crea un entorno virtual de Python que estará asociado para siempre con este proyecto. En lugar de crear una carpeta que contenga sus bibliotecas de dependencia (como hace virtualenv), Poetry crea un entorno en una ruta de sistema global, por lo tanto, separa el código fuente de dependencia de su proyecto. Una vez que se crea este entorno virtual, se puede activar de nuevo en cualquier momento simplemente ejecutando poetry shell en el directorio de su proyecto en el futuro. Intente comparar la salida que python antes y después de activar el shell de su proyecto para ver cómo Poetry maneja los entornos virtuales.


* **`poetry install`**: instala las dependencias especificadas en `pyproject.toml`. La primera vez que se instalan las dependencias de un proyecto, se crea un archivo `poetry.lock`, que contiene los números de versión reales de cada paquete que se instaló (es decir, si `Flask = "*"` resultó en la descarga de `Flask versión 1.0.0`, el número de versión real se almacenaría en `poetry.lock`). Si hay un archivo `poetry.lock`, los números de versión en `poetry.lock` siempre tendrán prioridad sobre lo que está en `pyproject.toml`. Cómo buena práctica, se debe mantener tanto `pyproject.toml` como `poetry.lock` en tu control de versión



* **`poetry update`**: imita la funcionalidad de `install`, con la excepción de que los números de versión en .lock NO se respetarán. Si existen versiones más nuevas para los paquetes en pyproject.toml, se instalarán versiones más nuevas y .lock se actualizará en consecuencia.


* **`poetry add [package-name]`**: Un atajo para agregar una dependencia a pyproject.toml. El paquete se instala inmediatamente después de agregarlo.


* **`poetry remove [package-name]`**: Lo contrario de lo anterior.


* **`poetry export -f requirements.txt> requirements.txt`**: exporta el contenido del archivo .lock de su proyecto a requirements.txt. Resulta útil cuando se entrega el trabajo a los desarrolladores que todavía usan requirements.txt por alguna razón.
