# Pyenv

## Introducción

Al instalar un sistema operativo, es probable que ya venga con una distribución de Python pre-existente; en el caso de distribuciones de Linux basadas en Debian y Mac ésta suele ser Python 2.7; otras traen tanto Python 2.7 como Python 3.6; mientras que Windows no trae Python pre-instalado. Éstas instalaciones suelen llamarse como _System Python_ o la versión de Python del sistema, y posee todas las librerias que el **Sistema Operativo** utiliza durante su funcionamiento regular.

Cuando queremos desarrollar software en Python, un riesgo que queremos mitigar es el de instalar erróneamente un paquete en _System Python_ y **modificar las librerias que el sistema operativo utiliza para su correcto funcionamiento**, lo que podría tener consecuencias catastróficas si no sabemos qué es lo que estamos haciendo.

Por otro lado además nos gustaría usar **distintas versiones de Python que la pre-instalada en el sistema operativo** y tener un mecanismo para instalar dependencias de forma segura sobre esas instalaciones específicas y no sobre _System Python_. Esto es especialmente importante cuando se desarrollan librerías que deben probarse para varias versiones de Python distintas de manera simultánea.
html_img_enable=true,
[Pyenv](https://github.com/pyenv/pyenv) nos ayuda a resolver ambos problemas, y entre sus utilidades nos permite instalar distintas versiones de Python en un directorio aparte, y además nos permite configurar que versión de Python deseamos usar para cada caso. 

Consiste en una herramienta que construye de manera nativa cualquier versión de Python desde su codigo fuente y luego la instala de manera local en el computador, permitiéndonos usar desde las versiones más antiguas de Python (por si tenemos que mantener algun desarrollo _legacy_) hasta las versiones más experimentales que aún no se consideran estables.

![pyenv](img/pyenv-pyramid.webp)

## Instalación

Para Linux y Mac lo recomendable es instalar todos los pre-requisitos guiándose en [esta guia](https://github.com/pyenv/pyenv/wiki/Common-build-problems#prerequisites) y luego usar [pyenv-installer](https://github.com/pyenv/pyenv-installer)

## Uso

Si necesitamos una versión de Python específica podemos usar pyenv para instalarla de la siguiente forma:

```bash
pyenv install <VERSION>
```

También podemos ver qué versiones de Python se pueden instalar usando:

```bash
pyenv install -l
```

La lista es muy bastante amplia; Miniconda, Anaconda, Cython, Pypy, Jython, entre otras están disponibles para ser instaladas usando esta herramienta.

## Recomendaciones

- Luego de instalar pyenv y alguna version de Python con la que te sientas cómodo, configura pyenv para que desde ahora en adelante cuando llames a Python (a menos que especifiques otra cosa) se llame siempre por defecto a la nueva instalación y no a _System Python_. Para esto ejecuta:
 ```bash
    pyenv global <VERSION>
 ```

- Una vez que tienes tu(s) intérprete(s) de Python listo(s), puedes manejar ambientes virtuales sobre estos mismos usando herramientas como Poetry o Pipenv.