# Documentar sus proyectos de Python
Los proyectos de Python vienen en todo tipo de formas, tamaños y propósitos. La forma en que documente su proyecto debe adaptarse a su situación específica. Ten en cuenta quiénes serán los usuarios de tu proyecto y adáptate a sus necesidades. Dependiendo del tipo de proyecto, se recomiendan ciertos aspectos de la documentación. El diseño general del proyecto y su documentación debe ser el siguiente:

```
project_root/
│
├── project/  # Project source code
├── docs/
├── README
├── HOW_TO_CONTRIBUTE
├── CODE_OF_CONDUCT
├── examples.py
```

Los proyectos se pueden subdividir generalmente en tres tipos principales: privados, compartidos y públicos / de código abierto.

## Proyectos privados
Los proyectos privados son proyectos destinados solo para uso personal y, por lo general, no se comparten con otros usuarios o desarrolladores. La documentación puede ser bastante liviana en este tipo de proyectos. Hay algunas partes recomendadas para agregar según sea necesario:

* `Readme.md`: un breve resumen del proyecto y su propósito. Incluya cualquier requisito especial para la instalación u operación del proyecto.
* `examples.py`: un archivo de secuencia de comandos de Python que brinda ejemplos simples de cómo usar el proyecto.

Recuerde, aunque los proyectos privados están destinados a usted personalmente, también se le considera un usuario. Piense en cualquier cosa que pueda resultarle confusa en el futuro y asegúrese de capturarla en comentarios, strings de documentos o el archivo `Readme.md`.

## Proyectos Compartidos

Los proyectos compartidos son proyectos en los que colaboras con algunas otras personas en el desarrollo y / o uso del proyecto. El "cliente" o usuario del proyecto sigue siendo usted mismo y los pocos que utilizan el proyecto también.

La documentación debe ser un poco más rigurosa de lo necesario para un proyecto privado, principalmente para ayudar a incorporar nuevos miembros al proyecto o alertar a los contribuyentes / usuarios de nuevos cambios en el proyecto. Algunas de las partes recomendadas para agregar al proyecto son las siguientes:

* `Readme.md`: un breve resumen del proyecto y su propósito. Incluya cualquier requisito especial para instalar u operar el proyecto. Además, agregue los cambios importantes desde la versión anterior.
* `examples.py`: un archivo de secuencia de comandos de Python que brinda ejemplos simples de cómo usar los proyectos.
* `How to Contribute`: esto debe incluir cómo los nuevos contribuyentes al proyecto pueden comenzar a contribuir.

## Proyectos públicos y de código abierto
Los proyectos públicos y de código abierto son proyectos que están destinados a ser compartidos con un gran grupo de usuarios y pueden involucrar a grandes equipos de desarrollo. Estos proyectos deben tener una prioridad tan alta en la documentación del proyecto como el desarrollo real del proyecto en sí. Algunas de las partes recomendadas para agregar al proyecto son las siguientes:

* `Readme.md`: un breve resumen del proyecto y su propósito. Incluya cualquier requisito especial para instalar u operar los proyectos. Además, agregue los cambios importantes desde la versión anterior. Finalmente, agregue enlaces a más documentación, informes de errores y cualquier otra información importante para el proyecto. Dan Bader ha elaborado un gran tutorial sobre todo lo que debería incluirse en su archivo Léame.

* `How to Contribute`: esto debe incluir cómo pueden ayudar los nuevos contribuyentes al proyecto. Esto incluye desarrollar nuevas funciones, solucionar problemas conocidos, agregar documentación, agregar nuevas pruebas o informar problemas.

* `Code of Conduct`: define cómo los demás colaboradores deben tratarse entre sí al desarrollar o utilizar su software. Esto también indica lo que sucederá si este código se rompe. Si está utilizando Github, se puede generar una plantilla de Código de conducta con la redacción recomendada. Especialmente para proyectos de código abierto, considere agregar esto.

* `License` : un archivo de texto sin formato que describe la licencia que está usando su proyecto. Especialmente para proyectos de código abierto, considere agregar esto.

* `docs`: una carpeta que contiene más documentación. La siguiente sección describe con más detalle qué se debe incluir y cómo organizar el contenido de esta carpeta.

## Las cuatro secciones principales de la carpeta de documentos

Daniele Procida dio una maravillosa [charla de PyCon 2017](https://www.youtube.com/watch?v=azf6yzuJt54&ab_channel=PyCon2017) y una [publicación de blog](https://documentation.divio.com/) posterior sobre la documentación de proyectos de Python. Él menciona que todos los proyectos deben tener las siguientes cuatro secciones principales para ayudarlo a enfocar su trabajo:

* `Tutorials`: lecciones que llevan al lector de la mano a través de una serie de pasos para completar un proyecto (o ejercicio significativo). Orientado al aprendizaje del usuario.
* `How-To Guides`: guías que llevan al lector a través de los pasos necesarios para resolver un problema común (recetas orientadas a problemas).
* `References`: Explicaciones que aclaran e iluminan un tema en particular. Orientado a la comprensión.
* `Explanations`: descripciones técnicas de la maquinaria y cómo operarla (clases clave, funciones, API, etc.). Piense en el artículo de la Enciclopedia.

La siguiente tabla muestra cómo todas estas secciones se relacionan entre sí, así como su propósito general:

|                       	| Most Useful When We’re Studying 	| Most Useful When We’re Coding 	|
|:---------------------:	|:-------------------------------:	|-------------------------------	|
|        Practical Step 	|            Tutorials            	|         How-To Guides         	|
| Theoretical Knowledge 	|           Explanation           	|           Reference           	|

Al final, desea asegurarse de que sus usuarios tengan acceso a las respuestas a cualquier pregunta que puedan tener. Al organizar su proyecto de esta manera, podrá responder esas preguntas fácilmente y en un formato en el que podrán navegar rápidamente.

## Herramientas y recursos de documentación
Documentar su código, especialmente los proyectos grandes, puede resultar abrumador. Afortunadamente, existen algunas herramientas y referencias para comenzar:

| Tool          	| Description                                                                                                                                           	|   	|
|---------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------	|---	|
| Sphinx        	| A collection of tools to auto-generate documentation in multiple formats                                                                              	|   	|
| Epydoc        	| A tool for generating API documentation for Python modules based on their docstrings                                                                  	|   	|
| Read The Docs 	| Automatic building, versioning, and hosting of your docs for you                                                                                      	|   	|
| Doxygen       	| A tool for generating documentation that supports Python as well as multiple other languages                                                          	|   	|
| MkDocs        	| A static site generator to help build project documentation using the Markdown language                                                               	|   	|
| pycco         	| A “quick and dirty” documentation generator that displays code and documentation side by side. Check out our tutorial on how to use it for more info. 	|   	|

Junto con estas herramientas, hay algunos tutoriales, videos y artículos adicionales que pueden ser útiles cuando está documentando su proyecto:

* [Carol Willing - Esfinge práctica - PyCon 2018](https://www.youtube.com/watch?v=0ROZRNZkPS8&ab_channel=PyCon2018)
* [Daniele Procida - Desarrollo basado en documentación - Lecciones del proyecto Django - PyCon 2016](https://www.youtube.com/watch?v=bQSR1UpUdFQ)
* [Eric Holscher - Documentar su proyecto con Sphinx y leer los documentos - PyCon 2016](https://www.youtube.com/watch?v=hM4I58TA72g&ab_channel=PyCon2016)
* [Titus Brown, Luiz Irber - Creación, construcción, prueba y documentación de un proyecto de Python: un CÓMO práctico - PyCon 2016](https://www.youtube.com/watch?t=6299&v=SUt3wT43AeM&feature=youtu.be&ab_channel=PyCon2016)
* [Documentación oficial de reStructuredText](https://docutils.sourceforge.io/rst.html)
* [Introducción al texto reestructurado de Sphinx](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)

A veces, la mejor forma de aprender es imitar a los demás. Aquí hay algunos ejemplos excelentes de proyectos que usan bien la documentación:

* **Django**: [Docs](https://docs.djangoproject.com/en/2.0/) ([Source](https://github.com/django/django/tree/main/docs))
* **Requests**: [Docs](https://docs.python-requests.org/en/master/) ([Source](https://github.com/psf/requests/tree/master/docs))
* **Click**: [Docs](https://click.palletsprojects.com/en/latest/) ([Source](https://github.com/pallets/click/tree/main/docs))
* **Pandas**: [Docs](https://pandas.pydata.org/pandas-docs/stable/) ([Source](https://github.com/pandas-dev/pandas/tree/master/doc))

## ¿Dónde empiezo?
La documentación de los proyectos tiene una progresión sencilla:

* Sin documentación
* Alguna documentación
* Documentación completa
* Buena documentacion
* Gran documentación

Si no sabe a dónde ir a continuación con su documentación, mire dónde se encuentra su proyecto ahora en relación con la progresión anterior. ¿Tienes alguna documentación? Si no es así, empiece por ahí. Si tiene algo de documentación pero le faltan algunos de los archivos clave del proyecto, comience agregando esos.

Al final, no se desanime ni se abrume por la cantidad de trabajo requerido para documentar el código. Una vez que comience a documentar su código, será más fácil continuar. No dude en comentar si tiene preguntas o comuníquese con el equipo de Real Python en las redes sociales y lo ayudaremos.

## Referencias

* [Documenting Python](https://devguide.python.org/documenting/)
* [Documenting Python Code: A Complete Guide](https://realpython.com/documenting-python-code/#documenting-your-python-projects)
