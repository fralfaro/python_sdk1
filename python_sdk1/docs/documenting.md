# Documentar mediante Docstrings
Ahora que hemos aprendido a comentar, profundicemos en la documentación de una base de código Python. En esta sección, aprenderá acerca de las docstrings y cómo usarlas para la documentación. Esta sección se divide en las siguientes subsecciones:

* **Docstrings Background**: información básica sobre cómo funcionan los docstrings internamente en Python
* **Docstring Types**: los distintos "tipos" de docstrings (función, clase, método de clase, módulo, paquete y secuencia de comandos)
* **Docstring Formats**: los diferentes "formatos" de docstrings (Google, NumPy / SciPy, reStructuredText y Epytext)

## Docstrings Background
La documentación de su código Python se centra en docstrings. Estas son strings integradas que, cuando se configuran correctamente, pueden ayudar a sus usuarios y a usted mismo con la documentación de su proyecto. Junto con los docstrings, Python también tiene la función incorporada `help()` que imprime el docstrings de los objetos en la consola. A continuación, se muestra un ejemplo rápido:

```python
>>> help(str)
```
```
Help on class str in module builtins:

class str(object)
 |  str(object='') -> str
 |  str(bytes_or_buffer[, encoding[, errors]]) -> str
 |
 |  Create a new string object from the given object. If encoding or
 |  errors are specified, then the object must expose a data buffer
 |  that will be decoded using the given encoding and error handler.
 |  Otherwise, returns the result of object.__str__() (if defined)
 |  or repr(object).
 |  encoding defaults to sys.getdefaultencoding().
 |  errors defaults to 'strict'.
 # Truncated for readability
```

¿Cómo se genera esta salida? Dado que todo en Python es un objeto, puede examinar el directorio del objeto usando el comando `dir()`. Hagámoslo y veamos qué encontramos:
```python
>>> dir(str)
```
```
['__add__', ..., '__doc__', ..., 'zfill'] # Truncated for readability
```

Dentro de la salida de ese directorio, hay una propiedad interesante, `__doc__`. Si examina esa propiedad, descubrirá esto:
```python
>>> print(str.__doc__)
```
```
str(object='') -> str
str(bytes_or_buffer[, encoding[, errors]]) -> str

Create a new string object from the given object. If encoding or
errors are specified, then the object must expose a data buffer
that will be decoded using the given encoding and error handler.
Otherwise, returns the result of object.__str__() (if defined)
or repr(object).
encoding defaults to sys.getdefaultencoding().
errors defaults to 'strict'.
```

¡Voilà! Descubrió dónde se almacenan las docstrings dentro del objeto. Esto significa que puede manipular directamente esa propiedad. Sin embargo, existen restricciones para las incorporaciones:

```python
>>> str.__doc__ = "I'm a little string doc! Short and stout; here is my input and print me for my out"
```

```
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can't set attributes of built-in/extension type 'str'
```
Se puede manipular cualquier otro objeto personalizado:

```python
def say_hello(name):
    print(f"Hello {name}, is it me you're looking for?")

say_hello.__doc__ = "A simple function that says hello... Richie style"
```

```python
>>> help(say_hello)
```

```
Help on function say_hello in module __main__:

say_hello(name)
    A simple function that says hello... Richie style
```

Python tiene una característica más que simplifica la creación de docstrings. En lugar de manipular directamente la propiedad `__doc__`, la ubicación estratégica del
ddocstrings directamente debajo del objeto establecerá automáticamente el valor `__doc__`. Esto es lo que sucede con el mismo ejemplo anterior:

```python
def say_hello(name):
    """A simple function that says hello... Richie style"""
    print(f"Hello {name}, is it me you're looking for?")
```

```python
>>> help(say_hello)
```
```
Help on function say_hello in module __main__:

say_hello(name)
    A simple function that says hello... Richie style
```

¡Ahí tienes! Ahora comprende el trasfondo de las docstrings. Ha llegado el momento de conocer los diferentes tipos de docstrings y la información que deben contener.

## Docstring Types
Las convenciones de strings de documentos se describen en [PEP 257](). Su propósito es proporcionar a los usuarios una breve descripción general del objeto. Deben mantenerse lo suficientemente concisos para que sean fáciles de mantener, pero igualmente elaborados para que los nuevos usuarios comprendan su propósito y cómo utilizar el objeto documentado.

En todos los casos, las docstrings deben usar el formato de string de comillas triples y dobles ("" "). Esto debe hacerse tanto si el docstrings tiene varias líneas como si no. Como mínimo, un docstrings debe ser un resumen rápido de lo que sea lo que está describiendo y debe estar contenido en una sola línea:
```python
"""This is a quick summary line used as a description of the object."""
```

Los docstrings de varias líneas se utilizan para desarrollar más el objeto más allá del resumen. Todas los docstrings de varias líneas tienen las siguientes partes:

* Una línea de resumen de una línea
* Una línea en blanco que precede al resumen
* Cualquier elaboración adicional para el docstrings
* Otra linea en blanco

```python
"""This is the summary line

This is the further elaboration of the docstring. Within this section,
you can elaborate further on details as appropriate for the situation.
Notice that the summary and the elaboration is separated by a blank new
line.
"""

# Notice the blank line above. Code should continue on this line.
```

Todas las docstrings deben tener la misma longitud máxima de caracteres que los comentarios (72 caracteres). Las docstrings se pueden dividir en tres categorías principales:

* **Class Docstrings**: Clase y métodos de clase
* **Package and Module Docstrings**: paquete, módulos y funciones
* **Script Docstrings**: Script y funciones

### Class Docstrings
Las docstrings de clase se crean para la propia clase, así como para cualquier método de clase. Los docstrings se colocan inmediatamente después de la clase o método de clase con sangría de un nivel:

```python
class SimpleClass:
    """Class docstrings go here."""

    def say_hello(self, name: str):
        """Class method docstrings go here."""

        print(f'Hello {name}')
```

Los docstrings de la clase deben contener la siguiente información:

* Un breve resumen de su propósito y comportamiento.
* Cualquier método público, junto con una breve descripción.
* Cualquier propiedad de clase (atributos)
* Cualquier cosa relacionada con la [interfaz](https://realpython.com/python-interface/) para subclases, si la clase está destinada a ser subclasificada.

Los parámetros del constructor de la clase deben documentarse dentro del docstrings del método de clase `__init__`. Los métodos individuales deben documentarse utilizando sus docstrings individuales. Los docstrings del método de clase deben contener lo siguiente:

* Una breve descripción de qué es el método y para qué se utiliza.
* Cualquier argumento (obligatorio y opcional) que se pase, incluidos los argumentos de palabras clave.
* Etiquete los argumentos que se consideren opcionales o que tengan un valor predeterminado
* Cualquier efecto secundario que se produzca al ejecutar el método.
* Cualquier excepción que se plantee
* Cualquier restricción sobre cuándo se puede llamar al método

Tomemos un ejemplo simple de una clase de datos que representa un animal. Esta clase contendrá algunas propiedades de clase, propiedades de instancia, un `__init__` y un [método de instancia](https://realpython.com/instance-class-and-static-methods-demystified/) único:

```python
class Animal:
    """
    A class used to represent an Animal

    ...

    Attributes
    ----------
    says_str : str
        a formatted string to print out what the animal says
    name : str
        the name of the animal
    sound : str
        the sound that the animal makes
    num_legs : int
        the number of legs the animal has (default 4)

    Methods
    -------
    says(sound=None)
        Prints the animals name and what sound it makes
    """

    says_str = "A {name} says {sound}"

    def __init__(self, name, sound, num_legs=4):
        """
        Parameters
        ----------
        name : str
            The name of the animal
        sound : str
            The sound the animal makes
        num_legs : int, optional
            The number of legs the animal (default is 4)
        """

        self.name = name
        self.sound = sound
        self.num_legs = num_legs

    def says(self, sound=None):
        """Prints what the animals name is and what sound it makes.

        If the argument `sound` isn't passed in, the default Animal
        sound is used.

        Parameters
        ----------
        sound : str, optional
            The sound the animal makes (default is None)

        Raises
        ------
        NotImplementedError
            If no sound is set for the animal or passed in as a
            parameter.
        """

        if self.sound is None and sound is None:
            raise NotImplementedError("Silent Animals are not supported!")

        out_sound = self.sound if sound is None else sound
        print(self.says_str.format(name=self.name, sound=out_sound))
```

### Package and Module Docstrings
Los docstrings del paquete deben colocarse en la parte superior del archivo `__init__.py` del paquete. Este docstrings debe enumerar los módulos y subpaquetes que exporta el paquete.

Las docstrings del módulo son similares a las docstrings de la clase. En lugar de documentar las clases y los métodos de clase, ahora es el módulo y las funciones que se encuentran dentro. Los docstrings del módulo se colocan en la parte superior del archivo incluso antes de cualquier importación. Los docstrings del módulo deben incluir lo siguiente:

* Una breve descripción del módulo y su propósito.
* Una lista de las clases, excepciones, funciones y cualquier otro objeto exportado por el módulo.

El docstrings de una función de módulo debe incluir los mismos elementos que un método de clase:

* Una breve descripción de qué es la función y para qué se utiliza.
* Cualquier argumento (obligatorio y opcional) que se pase, incluidos los argumentos de palabras clave.
* Etiquete los argumentos que se consideren opcionales
* Cualquier efecto secundario que se produzca al ejecutar la función.
* Cualquier excepción que se plantee
* Cualquier restricción sobre cuándo se puede llamar a la función

### Script Docstrings
Los scripts se consideran ejecutables de un solo archivo que se ejecutan desde la consola. Las docstrings para scripts se colocan en la parte superior del archivo y deben estar lo suficientemente bien documentadas para que los usuarios puedan tener una comprensión suficiente de cómo usar el script. Debería ser utilizable para su mensaje de "uso", cuando el usuario pasa incorrectamente un parámetro o usa la opción -h.

Si usa argparse, puede omitir la documentación específica del parámetro, suponiendo que se haya documentado correctamente en el parámetro de ayuda de la función argparser.parser.add_argument. Se recomienda utilizar `__doc__` para el parámetro de descripción dentro del constructor de `argparse.ArgumentParser`. Consulte nuestro [tutorial](https://realpython.com/comparing-python-command-line-parsing-libraries-argparse-docopt-click/) sobre bibliotecas de análisis de línea de comandos para obtener más detalles sobre cómo usar `argparse` y otros analizadores de línea de comando comunes.

Finalmente, cualquier importación personalizada o de terceros debe incluirse dentro de las docstrings para permitir a los usuarios saber qué paquetes pueden ser necesarios para ejecutar el script. A continuación, se muestra un ejemplo de una secuencia de comandos que se usa para imprimir simplemente los encabezados de columna de una hoja de cálculo:

```python
"""Spreadsheet Column Printer

This script allows the user to print to the console all columns in the
spreadsheet. It is assumed that the first row of the spreadsheet is the
location of the columns.

This tool accepts comma separated value files (.csv) as well as excel
(.xls, .xlsx) files.

This script requires that `pandas` be installed within the Python
environment you are running this script in.

This file can also be imported as a module and contains the following
functions:

    * get_spreadsheet_cols - returns the column headers of the file
    * main - the main function of the script
"""

import argparse

import pandas as pd


def get_spreadsheet_cols(file_loc, print_cols=False):
    """Gets and prints the spreadsheet's header columns

    Parameters
    ----------
    file_loc : str
        The file location of the spreadsheet
    print_cols : bool, optional
        A flag used to print the columns to the console (default is
        False)

    Returns
    -------
    list
        a list of strings used that are the header columns
    """

    file_data = pd.read_excel(file_loc)
    col_headers = list(file_data.columns.values)

    if print_cols:
        print("\n".join(col_headers))

    return col_headers


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'input_file',
        type=str,
        help="The spreadsheet file to pring the columns of"
    )
    args = parser.parse_args()
    get_spreadsheet_cols(args.input_file, print_cols=True)


if __name__ == "__main__":
    main()
```

## Docstring Formats
Es posible que haya notado que, a lo largo de los ejemplos proporcionados en este tutorial, ha habido un formato específico con elementos comunes: argumentos, devoluciones y atributos. Hay formatos de docstrings específicos que se pueden usar para ayudar a los usuarios y a los analizadores de docstrings a tener un formato familiar y conocido. El formato utilizado en los ejemplos de este tutorial son docstrings de estilo NumPy / SciPy. Algunos de los formatos más habituales son los siguientes:

| Formatting Type        	| Description                                                                    	| Supported by Sphynx 	| Formal Specification 	|
|------------------------	|--------------------------------------------------------------------------------	|:-------------------:	|:--------------------:	|
| Google docstrings      	| Google’s recommended form of documentation                                     	|         Yes         	|          No          	|
| reStructuredText       	| Official Python documentation standard; Not beginner friendly but feature rich 	|         Yes         	|          Yes         	|
| NumPy/SciPy docstrings 	| NumPy’s combination of reStructuredText and Google Docstrings                  	|         Yes         	|          Yes         	|
| Epytext                	| A Python adaptation of Epydoc; Great for Java developers                       	|    Not officially   	|          Yes         	|

La selección del formato de docstrings depende de usted, pero debe seguir con el mismo formato en todo su documento / proyecto. Los siguientes son ejemplos de cada tipo para darle una idea de cómo se ve cada formato de documentación.

* **Google Docstrings Example**

```python
"""Gets and prints the spreadsheet's header columns

Args:
    file_loc (str): The file location of the spreadsheet
    print_cols (bool): A flag used to print the columns to the console
        (default is False)

Returns:
    list: a list of strings representing the header columns
"""
```

* **reStructuredText Example**

```python
"""Gets and prints the spreadsheet's header columns

:param file_loc: The file location of the spreadsheet
:type file_loc: str
:param print_cols: A flag used to print the columns to the console
    (default is False)
:type print_cols: bool
:returns: a list of strings representing the header columns
:rtype: list
"""
```

* **NumPy/SciPy Docstrings Example**

```python
"""Gets and prints the spreadsheet's header columns

Parameters
----------
file_loc : str
    The file location of the spreadsheet
print_cols : bool, optional
    A flag used to print the columns to the console (default is False)

Returns
-------
list
    a list of strings representing the header columns
"""
```

* **Epytext Example**

```python
"""Gets and prints the spreadsheet's header columns

@type file_loc: str
@param file_loc: The file location of the spreadsheet
@type print_cols: bool
@param print_cols: A flag used to print the columns to the console
    (default is False)
@rtype: list
@returns: a list of strings representing the header columns
"""
```