# HOME
Conceptos básicos sobre el diseño de software (version control, testing & automatic build management)

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/FAAM/python_sdk1), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/FAAM/python_sdk1
```

## Contenidos

```{tableofcontents}
```