# Conceptos básicos sobre el diseño de software 

<a href="https://fralfaro.gitlab.io/python_sdk1/"><img alt="Link a la Documentación" src="https://jupyterbook.org/badge.svg"></a>
[![pipeline status](https://gitlab.com/FAAM/python_sdk1/badges/master/pipeline.svg)](https://gitlab.com/FAAM/python_sdk1/-/commits/master)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/FAAM%2Fpython_sdk1/HEAD)


## Contenidos temáticos
* Terminal
* Git/Github
* OOP
* Ecosistema
* Code Quality
* Testing
* Documentación





